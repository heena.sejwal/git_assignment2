				GIT Assignment 2
			====================================
	
- Consider working on a file for which you maintain a document as well in which you are keeping the steps that you are performing for troubleshooting and you have accidentally added that file in your local repository and you have to get that file out from your local but not from your file system. Use the git operations suitable for this scenario.

In this case first step i followed is to create a file named as troubleshootingsteps and then added that file in staging area and then committed the file.

![Alt text](images/1.png?raw=true "dashboard Result") 


Next i used git reset to bring the file from local repository to the file system.

![Alt text](images/1.2.png?raw=true "dashboard Result")

- Consider Creating a html file to display the Name of the ninja and the batch number after the html is created and pushed, create a readme file for that html but after adding it you realized that we also have to mention the topics covered in the html file and we want to put those changes in the same last commit where html was originally pushed. Use the git operations suitable for this scenario.

here i created ninjainfo.html and add info of ninja's and the added the file in staging area and then commited and pushed the file.


![Alt text](images/2.1.png?raw=true "dashboard Result")


after the file has been pushed to remote repository, i created readme file for that html file and added the readme file and committed it 

![Alt text](images/2.2.png?raw=true "dashboard Result")



now i used git commit --amend command to add the readme file in the previous commit id instead of creating the new commit

![Alt text](images/2.3.png?raw=true "dashboard Result")


- Consider the scenario where we have create a feature branch from our master branch where you have to create the html file to display ninja's name and batch details, but you realized that the readme has to go first in order for any member as you are already in between the creation of your html file you cannot send it to your remote branch, come up with a strategy where you can preserve your changes and first create them readme file and only after the readme is pushed then you resume your work back on the html file, use the appropriate git operations for the same

In this case i created ninja branch and started working on ninja.html file then i added the file in staging area, and then i realized that readme file has to go first so i stopped my work on ninja.html file by saving that file using git stash command 

![Alt text](images/3.1.png?raw=true "dashboard Result")

after that i went back to master branch and created readme.md file in master branch, added it in staging area,committing it and after pushing it to the remote repo

![Alt text](images/3.2.png?raw=true "dashboard Result")

i jumped back to my feature branch and unstash my latest stash file and then again started working on my ninja.html file

![Alt text](images/3.3.png?raw=true "dashboard Result")



